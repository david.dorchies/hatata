#' Get column "Différentiel" content of a "pointage" page
#'
#' @param session a [rvest::session] provided by [login] or [jump_to_mon_calendrier] functions
#' @param date a [character] representing the current date in format "%Y-%m-%d"
#'
#' @return A named [vector] of [character] : the name is the date in "%d/%m/%Y" format and the content is the difference between theoretical and actually worked hours in format "(-)00h00".
#' @export
#'
#' @examples
#' \dontrun{
#' library(hatata)
#' session <- login("pnom", "password")
#' fdt_get_differentiel(session)
#' }
fdt_get_differentiel <- function(session,
                              date = format(Sys.Date() - months(1), "%Y-%m-%d")) {

  date <- as.Date(date)
  firstMonth <- lubridate::floor_date(date, "month")
  lastMonth <- lubridate::ceiling_date(date, "month") - 1
  dates <- seq(firstMonth, lastMonth, by = "1 day")

  session <- jump_to_pointage(session, date = date)
  df <- session %>%
    rvest::html_node(xpath = "//*[@id=\"fiche_pointage\"]") %>%
    rvest::html_table()

  differentiel <- unlist(df[-nrow(df),ncol(df)])
  names(differentiel) <- format(dates, "%d/%m/%Y")
  return(differentiel)
}
