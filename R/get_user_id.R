#' Get user_id from html session
#'
#' The session must have been logged and "jumped to time" page.
#'
#' @param session a [rvest::session] provided by [jump_to_mon_calendrier] function
#'
#' @return a [character] with the user id (ex: "123")
#' @export
#'
#' @examples
#' \dontrun{
#' library(hatata)
#' session <- login("pnom", "password") %>% jump_to_mon_calendrier()
#' get_user_id(session)
#' }
get_user_id <- function(session) {
  if(!inherits(session, "hatata_mon_calendrier")) {
    stop("`session` must be computed by `jump_to_mon_calendrier` function")
  }
  stringr::str_match(session$response$url, "user_id=([0-9]+)")[1,2]
}
