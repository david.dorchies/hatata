
# Hatata: HAsh TAg Temps Automatique

<!-- badges: start -->
<!-- badges: end -->

Le but du package R Hatata est de fournir les outils permettant de saisir automatiquement des temps dans [l'outil #temps](https://temps-activites.inra.fr/temps) utilisé à INRAE pour enregistrer le temps de travail des agents.

## Installation

Le package s'installe à partir des sources présentes sur le Gitlab Irstea&nbsp;:

``` r
install.packages("remotes")
remotes::install_gitlab("david.dorchies/hatata", host = "gitlab.irstea.fr")
```

## Exemple d'utilisation

Chargement de la librarie&nbsp;:

``` r
library(hatata)
```

Pour saisir des temps dans [l'outil #temps](https://temps-activites.inra.fr/temps), il faut d'abord créer une variable de session contenant les identifiants de connexion.

``` r
session <- login("mon_idenfiant_INRAE_pnom", "mon_mot_de_passe_INRAE")
```

Pour obtenir les dates du mois précédent pour lesquelles vous avez un nombre d'heures inférieur aux horaires théoriques et le nombre d'heures par jour à compléter:

``` r
> fdt_get_lack_days(session)
08/06/2021 10/06/2021 14/06/2021 15/06/2021 17/06/2021 21/06/2021 22/06/2021 24/06/2021 28/06/2021 29/06/2021
  "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"   "+07h44"
```

Pour saisir automatiquement ces temps dans la feuille de pointage :

``` r
fdt_add_events(session, fdt_get_lack_days(session))
```

Pour valider la feuille de temps mensuelle du mois de juin 2021&nbsp;:

``` r
fdt_valid(session, "01/06/2021")
```

# Roadmap

Pour rendre cette application fonctionnelle pour le commun des mortels, il faut une interface graphique&nbsp;:

- Développement d'une interface Shiny pour les fonctionnalités développées

Saisie des congés :

- Automatiser l'annulation des jours de télétravail ou autre lorsque l'on veut poser des congés

Saisie des activités&nbsp;:

- Lire les temps d'activité à partir de l'agenda de l'agent ou d'un fichier Excel
- Affecter les temps en fonction de mots clés prédéfinis pour chaque affectation
- Saisir automatiquement ces informations dans l'interface

# Contribution

Toutes les contributions sont les bienvenues pour aboutir à un outil fonctionnel pour le plus grand nombre :)
